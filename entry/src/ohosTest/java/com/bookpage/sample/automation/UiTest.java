package com.bookpage.sample.automation;

import com.anlia.pageturn.bean.MyPoint;
import com.anlia.pageturn.view.BookPageView;
import com.anlia.pageturn.view.CoverPageView;
import com.bookpage.sample.BookPageViewAbility;
import com.bookpage.sample.CoverPageViewAbility;
import com.bookpage.sample.ResourceTable;
import ohos.aafwk.ability.Ability;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class UiTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        delayTwoSeconds();
    }

    @Test
    public void TestCoverPageViewLeftLimit() throws NoSuchFieldException, IllegalAccessException {
        Ability coverPageViewAbility = EventHelper.startAbility(CoverPageViewAbility.class);
        delayTwoSeconds();
        CoverPageView coverPageView = (CoverPageView) coverPageViewAbility
                .findComponentById(ResourceTable.Id_view_cover_page);
        Field field = CoverPageView.class.getDeclaredField("pageNum");
        field.setAccessible(true);
        int pageNum1 = (int) field.get(coverPageView);
        assertEquals("It's right", 1, pageNum1);
        int touchStartWidth = coverPageView.getWidth() / 4;
        int touchStopWidth = coverPageView.getWidth() * 3 / 4;
        int touchHeight = coverPageView.getHeight() / 2;
        EventHelper.inputSwipe(coverPageViewAbility, touchStartWidth, touchHeight, touchStopWidth, touchHeight, 1000);
        int pageNum2 = (int) field.get(coverPageView);
        assertEquals("It's right", pageNum1, pageNum2);
        delayTwoSeconds();
    }

    @Test
    public void TestCoverPageViewNormalSwitch() throws NoSuchFieldException, IllegalAccessException {
        Ability coverPageViewAbility = EventHelper.startAbility(CoverPageViewAbility.class);
        delayTwoSeconds();
        CoverPageView coverPageView = (CoverPageView) coverPageViewAbility
                .findComponentById(ResourceTable.Id_view_cover_page);
        Field field = CoverPageView.class.getDeclaredField("pageNum");
        field.setAccessible(true);
        int pageNum1 = (int) field.get(coverPageView);
        assertEquals("It's right", 1, pageNum1);
        int touchStartWidth = coverPageView.getWidth() * 3 / 4;
        int touchStopWidth = coverPageView.getWidth() / 4;
        int touchHeight = coverPageView.getHeight() / 2;
        EventHelper.inputSwipe(coverPageViewAbility, touchStartWidth, touchHeight, touchStopWidth, touchHeight, 1000);
        int pageNum2 = (int) field.get(coverPageView);
        assertEquals("It's right", 2, pageNum2);
        delayTwoSeconds();
    }

    @Test
    public void TestCoverPageViewRightLimit() throws NoSuchFieldException, IllegalAccessException {
        Ability coverPageViewAbility = EventHelper.startAbility(CoverPageViewAbility.class);
        delayTwoSeconds();
        CoverPageView coverPageView = (CoverPageView) coverPageViewAbility
                .findComponentById(ResourceTable.Id_view_cover_page);
        Field field = CoverPageView.class.getDeclaredField("pageNum");
        field.setAccessible(true);
        int pageNum1 = (int) field.get(coverPageView);
        assertEquals("It's right", 1, pageNum1);
        int touchStartWidth = coverPageView.getWidth() * 3 / 4;
        int touchStopWidth = coverPageView.getWidth() / 4;
        int touchHeight = coverPageView.getHeight() / 2;
        EventHelper.inputSwipe(coverPageViewAbility, touchStartWidth, touchHeight, touchStopWidth, touchHeight, 1000);
        EventHelper.inputSwipe(coverPageViewAbility, touchStartWidth, touchHeight, touchStopWidth, touchHeight, 1000);
        int pageNum2 = (int) field.get(coverPageView);
        assertEquals("It's right", 3, pageNum2);
        EventHelper.inputSwipe(coverPageViewAbility, touchStartWidth, touchHeight, touchStopWidth, touchHeight, 1000);
        int pageNum3 = (int) field.get(coverPageView);
        assertEquals("It's right", 3, pageNum3);
        delayTwoSeconds();
    }


    @Test
    public void TestBookPageView() throws NoSuchFieldException, IllegalAccessException {
        Ability bookPageViewAbility = EventHelper.startAbility(BookPageViewAbility.class);
        delayTwoSeconds();
        BookPageView coverPageView = (BookPageView) bookPageViewAbility
                .findComponentById(ResourceTable.Id_view_book_page);
        Field field = BookPageView.class.getDeclaredField("a");
        field.setAccessible(true);
        MyPoint myPointAStart = (MyPoint) field.get(coverPageView);
        assertTrue("It's right", myPointAStart.x == -1 && myPointAStart.y == -1);
        int touchStartWidth = coverPageView.getWidth() * 3 / 4;
        int touchStopWidth = coverPageView.getWidth() / 2;
        int touchStartHeight = coverPageView.getHeight() * 3 / 4;
        int touchStopHeight = coverPageView.getHeight() / 2;
        EventHelper.inputSwipe(bookPageViewAbility, touchStartWidth, touchStartHeight, touchStopWidth, touchStopHeight, 1000);
        MyPoint myPointAStop = (MyPoint) field.get(coverPageView);
        assertTrue("It's right", myPointAStop.x == -1 && myPointAStop.y == -1);
        delayTwoSeconds();
    }

    private void delayTwoSeconds() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}