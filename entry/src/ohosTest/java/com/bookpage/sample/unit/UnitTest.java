package com.bookpage.sample.unit;


import com.anlia.pageturn.factory.PageFactory;
import com.anlia.pageturn.factory.PicturesPageFactory;
import com.anlia.pageturn.utils.BitmapUtils;
import com.anlia.pageturn.utils.ScreenUtils;
import com.anlia.pageturn.utils.ViewUtils;
import com.anlia.pageturn.view.CoverPageView;
import com.bookpage.sample.ResourceTable;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.PixelMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Optional;

import static org.junit.Assert.*;

public class UnitTest {

    private Context context;
    private AttrSet attrSet;

    @Before
    public void setUp() {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        attrSet = new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        };
    }

    @After
    public void tearDown() {
        context = null;
        attrSet = null;
    }

    @Test
    public void coverPageViewSetPageFactory() throws NoSuchFieldException, IllegalAccessException {
        CoverPageView coverPageView = new CoverPageView(context, attrSet);
        int[] pIds = new int[]{ResourceTable.Media_page1, ResourceTable.Media_page2, ResourceTable.Media_page3};
        coverPageView.setPageFactory(new PicturesPageFactory(context, pIds));
        Field field = CoverPageView.class.getDeclaredField("pageFactory");
        field.setAccessible(true);
        PageFactory pageFactory = (PageFactory) field.get(coverPageView);
        assertNotNull("It's right", pageFactory);
    }

    @Test
    public void viewUtilsMeasureSize() throws NoSuchFieldException, IllegalAccessException {
        int result = ViewUtils.measureSize(600, 1073742844);
        assertTrue("It's right", result > 0);
    }

    @Test
    public void screenUtilsGetScreenWidth() {
        int screenWidth = ScreenUtils.getScreenWidth(context);
        assertTrue("It's right", screenWidth > 0);
    }

    @Test
    public void screenUtilsGetScreenHeight() {
        int screenHeight = ScreenUtils.getScreenHeight(context);
        assertTrue("It's right", screenHeight > 0);
    }

    @Test
    public void bitmapUtilsResourceToBitmap() {
        Resource resource = null;
        try {
            resource = context.getResourceManager().getResource(ResourceTable.Media_page1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int newW = 100;
        int newH = 100;
        PixelMap pixelMap = BitmapUtils.resourceToBitmap(resource, newW, newH);
        assertTrue("It's right", pixelMap.getImageInfo().size.width == newW
                && pixelMap.getImageInfo().size.height == newH);
    }

    @Test
    public void PicturesPageFactoryHetBitmapByIndex() throws NoSuchFieldException, IllegalAccessException {
        int[] pIds = new int[]{ResourceTable.Media_page1, ResourceTable.Media_page2, ResourceTable.Media_page3};
        PicturesPageFactory picturesPageFactory = new PicturesPageFactory(context, pIds);
        Field field = PicturesPageFactory.class.getDeclaredField("style");
        field.setAccessible(true);
        int style = (int) field.get(picturesPageFactory);
        assertEquals("It's right", PicturesPageFactory.STYLE_IDS, style);
        PixelMap pixelMapEffective = picturesPageFactory.getBitmapByIndex(0);
        assertNotNull("It's right", pixelMapEffective);
    }

}