package com.bookpage.sample;

import com.bookpage.sample.slice.BookPageViewAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class BookPageViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(BookPageViewAbilitySlice.class.getName());
    }
}
