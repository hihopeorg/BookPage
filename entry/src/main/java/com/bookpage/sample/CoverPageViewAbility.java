package com.bookpage.sample;

import com.bookpage.sample.slice.CoverPageViewAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CoverPageViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CoverPageViewAbilitySlice.class.getName());
    }
}
