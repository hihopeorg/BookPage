package com.bookpage.sample.slice;

;
import com.bookpage.sample.CoverPageViewAbility;
import com.bookpage.sample.BookPageViewAbility;
import com.bookpage.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_buttonCoverPageView).setClickedListener(component -> {
            startAbility(CoverPageViewAbility.class.getSimpleName());
        });

        findComponentById(ResourceTable.Id_buttonBookPageView).setClickedListener(component -> {
            startAbility(BookPageViewAbility.class.getSimpleName());
        });
    }

    private void startAbility(String abilityNam) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(abilityNam)
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }
}
