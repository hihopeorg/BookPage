package com.bookpage.sample.slice;

import com.anlia.pageturn.factory.PicturesPageFactory;
import com.anlia.pageturn.view.CoverPageView;
import com.bookpage.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

public class CoverPageViewAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_cover_page_view);

        int[] pIds = new int[]{ResourceTable.Media_page1, ResourceTable.Media_page2, ResourceTable.Media_page3};
        CoverPageView coverPageView = (CoverPageView) findComponentById(ResourceTable.Id_view_cover_page);
        coverPageView.setPageFactory(new PicturesPageFactory(this, pIds));
    }
}
