# BookPage

**本项目是基于开源项目BookPage进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/AnliaLee/BookPage ）追踪到原项目版本**

#### 项目介绍

- 项目名称：自定义翻页效果

- 所属系列：ohos的第三方组件适配移植

- 功能：

  1. 创建覆盖翻页效果
  2. 创建仿真书籍翻页效果
  
- 项目移植状态：完成

- 调用差异：无

- 项目作者和维护人：hihope

- 联系方式：hihope@hoperun.com

- 原项目Doc地址：https://github.com/AnliaLee/BookPage

- 原项目基线版本：无

- 编程语言：Java

#### 效果展示
![avatar](screenshot/效果展示.gif)

#### 安装教程

方法1.

1. 添加har包到lib文件夹内。

2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
}
```
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/'
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.anlia.pageturn.ohos:bookpage:1.0.1'
}
```

#### 使用说明

1. ##### 创建覆盖翻页效果

   ```
    <com.anlia.pageturn.view.CoverPageView
       ohos:id="$+id:view_cover_page"
       ohos:height="match_parent"
       ohos:width="match_parent"
       ohos:margin="10vp"/>
   ```
   
   ```
   int[] pIds = new int[]{ResourceTable.Media_page1, ResourceTable.Media_page2, ResourceTable.Media_page3};
   CoverPageView coverPageView = (CoverPageView) findComponentById(ResourceTable.Id_view_cover_page);
   coverPageView.setPageFactory(new PicturesPageFactory(this, pIds));
   ```
   
3. ##### 创建仿真书籍翻页效果

   ```
    <com.anlia.pageturn.view.BookPageView
       ohos:id="$+id:view_book_page"
       ohos:height="match_parent"
       ohos:width="match_parent"
       ohos:margin="10vp"/>
   ```
   
#### 版本迭代

- v1.0.1

#### 版权和许可信息

- MIT License
