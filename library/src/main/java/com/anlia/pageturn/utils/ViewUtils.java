package com.anlia.pageturn.utils;

import ohos.agp.components.Component;

/**
 * Created by anlia on 2017/12/12.
 */

public class ViewUtils {

    /**
     * 测量、设置View默认宽高
     */
    public static int measureSize(int defaultSize, int measureSpec) {
        int result = defaultSize;
        int specMode = Component.EstimateSpec.getMode(measureSpec);
        int specSize = Component.EstimateSpec.getSize(measureSpec);

        if (specMode == Component.EstimateSpec.PRECISE) {
            result = specSize;
        } else if (specMode == Component.EstimateSpec.NOT_EXCEED) {
            result = Math.min(result, specSize);
        }
        return result;
    }
}
