package com.anlia.pageturn.utils;

import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

/**
 * Created by anlia on 2017/12/11.
 */

public class BitmapUtils {
    /**
     * drawable图片资源转pixelMap
     */
    public static PixelMap drawableToBitmap(PixelMapElement drawable) {
        return drawable.getPixelMap();
    }

    /**
     * resource图片资源转pixelMap并重置宽高
     */
    public static PixelMap resourceToBitmap(Resource resource, int newW, int newH) {
        PixelMap pixelMap = ImageSource.create(resource, new ImageSource.SourceOptions()).createPixelmap(new ImageSource.DecodingOptions());
        return changeBitmapSize(pixelMap, newW, newH);
    }

    /**
     * 改变bitmap的大小
     */
    public static PixelMap changeBitmapSize(PixelMap bitmap, int newW, int newH) {
        // 得到新的图片
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(newW, newH);
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        initializationOptions.editable = true;
        bitmap = PixelMap.create(bitmap, initializationOptions);
        return bitmap;
    }
}
