package com.anlia.pageturn.view;

import com.anlia.pageturn.bean.MyPoint;
import com.anlia.pageturn.factory.PageFactory;
import com.anlia.pageturn.utils.ViewUtils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ScrollHelper;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by anlia on 2017/12/11.
 */

public class CoverPageView extends Component implements Component.TouchEventListener, Component.DrawTask,
        Component.EstimateSizeListener {
    private int defaultWidth;//默认宽度
    private int defaultHeight;//默认高度
    private int viewWidth;
    private int viewHeight;
    private int pageNum;//当前页数
    private float xDown;//记录初始触摸的x坐标
    private float scrollPageLeft;//滑动页左边界

    private PageFactory pageFactory;
    private MyPoint touchPoint;//触摸点
    private ScrollHelper mScroller;
    private ShapeElement shadowDrawable;

    private PixelMap previousPage;//上一页bitmap
    private PixelMap currentPage;//当前页bitmap
    private PixelMap nextPage;//下一页bitmap

    boolean mIsFirstDraw = true;//首次绘制

    private int touchStyle;//触摸类型
    public static final int TOUCH_LEFT = 1;//点击左边区域
    public static final int TOUCH_RIGHT = 2;//点击右边区域

    private int pageState;//翻页状态，用于限制翻页动画结束前的触摸操作
    public static final int PAGE_STAY = 0;//处于静止状态
    public static final int PAGE_NEXT = 1;//翻至下一页
    public static final int PAGE_PREVIOUS = 2;//翻至上一页

    private final Paint nullPaint = new Paint();

    public CoverPageView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setTouchEventListener(this);
        addDrawTask(this);
        setEstimateSizeListener(this);

        defaultWidth = 600;
        defaultHeight = 1000;
        pageNum = 1;
        scrollPageLeft = 0;
        touchStyle = TOUCH_RIGHT;
        pageState = PAGE_STAY;

        touchPoint = new MyPoint(-1, -1);
        mScroller = new ScrollHelper();

        RgbColor[] mBackShadowColors = new RgbColor[]{RgbColor.fromArgbInt(0x66000000), RgbColor.fromArgbInt(0x00000000)};
        shadowDrawable = new ShapeElement();
        shadowDrawable.setGradientOrientation(ShapeElement.Orientation.LEFT_TO_RIGHT);
        shadowDrawable.setRgbColors(mBackShadowColors);
        shadowDrawable.setShaderType(ShapeElement.LINEAR_GRADIENT_SHADER_TYPE);
    }

    /**
     * 设置工厂类
     */
    public void setPageFactory(final PageFactory factory) {
        if (factory.hasData) {
            pageFactory = factory;
            mIsFirstDraw = true;
        } else {
            mIsFirstDraw = false;
        }
    }

    @Override
    public boolean onEstimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        viewWidth = ViewUtils.measureSize(defaultWidth, widthEstimatedConfig);
        viewHeight = ViewUtils.measureSize(defaultHeight, heightEstimatedConfig);
        setEstimatedSize(
                EstimateSpec.getSizeWithMode(viewWidth, Component.EstimateSpec.PRECISE),
                EstimateSpec.getSizeWithMode(viewHeight, Component.EstimateSpec.PRECISE));
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(viewWidth, viewHeight);
        initializationOptions.pixelFormat = PixelFormat.RGB_565;
        initializationOptions.editable = true;
        previousPage = PixelMap.create(initializationOptions);
        currentPage = PixelMap.create(initializationOptions);
        nextPage = PixelMap.create(initializationOptions);
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (pageFactory != null) {
            if (mIsFirstDraw) {
                pageFactory.drawCurrentBitmap(currentPage, pageNum);
                mIsFirstDraw = false;
            }
            if (touchPoint.x == -1 && touchPoint.y == -1) {
                drawCurrentPage(canvas);
                pageState = PAGE_STAY;
            } else {
                if (touchStyle == TOUCH_RIGHT) {
                    drawCurrentPage(canvas);
                    drawPreviousPage(canvas);
                } else {
                    drawNextPage(canvas);
                    drawCurrentPage(canvas);
                }
                drawShadow(canvas);
            }
        }
    }

    /**
     * 绘制上一页
     */
    private void drawPreviousPage(Canvas canvas) {
        canvas.drawPixelMapHolder(new PixelMapHolder(previousPage), scrollPageLeft, 0, nullPaint);
    }

    /**
     * 绘制当前页
     */
    private void drawCurrentPage(Canvas canvas) {
        if (touchStyle == TOUCH_RIGHT) {
            canvas.drawPixelMapHolder(new PixelMapHolder(currentPage), 0, 0, nullPaint);
        } else if (touchStyle == TOUCH_LEFT) {
            canvas.drawPixelMapHolder(new PixelMapHolder(currentPage), scrollPageLeft, 0, nullPaint);
        }
    }

    /**
     * 绘制下一页
     */
    private void drawNextPage(Canvas canvas) {
        canvas.drawPixelMapHolder(new PixelMapHolder(nextPage), 0, 0, nullPaint);
    }

    /**
     * 绘制阴影
     */
    private void drawShadow(Canvas canvas) {
        int left = (int) (viewWidth + scrollPageLeft);
        shadowDrawable.setBounds(left, 0, left + 30, viewHeight);
        shadowDrawable.drawToCanvas(canvas);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        MmiPoint point = event.getPointerPosition(event.getIndex());
        float x = point.getX();
        float y = point.getY();
        if (pageState == PAGE_STAY) {
            switch (event.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    xDown = x;
                    if (x <= viewWidth / 2.0) {//左
                        touchStyle = TOUCH_LEFT;
                        if (pageNum > 1) {
                            pageNum--;
                            pageFactory.drawCurrentBitmap(currentPage, pageNum);
                            pageFactory.drawNextBitmap(nextPage, pageNum);
                            pageNum++;
                        }
                    } else {//右
                        touchStyle = TOUCH_RIGHT;
                        if (pageNum < pageFactory.pageTotal) {
                            pageNum++;
                            pageFactory.drawPreviousBitmap(previousPage, pageNum);
                            pageFactory.drawCurrentBitmap(currentPage, pageNum);
                            pageNum--;
                        }
                    }
                    break;
                case TouchEvent.POINT_MOVE:
                    if (touchStyle == TOUCH_LEFT) {
                        if (pageNum > 1) {
                            scrollPage(x, y);
                        }
                    } else if (touchStyle == TOUCH_RIGHT) {
                        if (pageNum < pageFactory.pageTotal) {
                            scrollPage(x, y);
                        }
                    }
                    break;
                case TouchEvent.PRIMARY_POINT_UP:
                    autoScroll();
                    computeScroll();
                    break;
            }
        }
        return true;
    }

    public void computeScroll() {
        if (touchStyle == TOUCH_RIGHT && pageNum < pageFactory.pageTotal) {
            pageNum++;
        } else if (touchStyle == TOUCH_LEFT && pageNum > 1) {
            pageNum--;
        }
        resetView();
    }

    /**
     * 计算滑动页面左边界位置，实现滑动当前页效果
     */
    private void scrollPage(float x, float y) {
        touchPoint.x = x;
        touchPoint.y = y;

        if (touchStyle == TOUCH_RIGHT) {
            scrollPageLeft = touchPoint.x - xDown;
        } else if (touchStyle == TOUCH_LEFT) {
            scrollPageLeft = touchPoint.x - xDown - viewWidth;
        }

        if (scrollPageLeft > 0) {
            scrollPageLeft = 0;
        }
        invalidate();
    }

    /**
     * 自动完成滑动操作
     */
    private void autoScroll() {
        switch (touchStyle) {
            case TOUCH_LEFT:
                if (pageNum > 1) {
                    autoScrollToPreviousPage();
                }
                break;
            case TOUCH_RIGHT:
                if (pageNum < pageFactory.pageTotal) {
                    autoScrollToNextPage();
                }
                break;
        }
    }

    /**
     * 自动完成翻到下一页操作
     */
    private void autoScrollToNextPage() {
        pageState = PAGE_NEXT;

        int dx, dy;
        dx = (int) -(viewWidth + scrollPageLeft);
        dy = (int) (touchPoint.y);

        mScroller.startScroll((int) (viewWidth + scrollPageLeft), (int) touchPoint.y, dx, dy);
    }

    /**
     * 自动完成返回上一页操作
     */
    private void autoScrollToPreviousPage() {
        pageState = PAGE_PREVIOUS;

        int dx, dy;
        dx = (int) -scrollPageLeft;
        dy = (int) (touchPoint.y);

        mScroller.startScroll((int) (viewWidth + scrollPageLeft), (int) touchPoint.y, dx, dy);
    }

    /**
     * 取消翻页动画,计算滑动位置与时间
     */
    private void startCancelAnim() {
        int dx, dy;
        dx = (int) (viewWidth - 1 - touchPoint.x);
        dy = (int) (touchPoint.y);
        mScroller.startScroll((int) touchPoint.x, (int) touchPoint.y, dx, dy);
    }

    /**
     * 重置操作
     */
    private void resetView() {
        scrollPageLeft = 0;
        touchPoint.x = -1;
        touchPoint.y = -1;
        invalidate();
    }
}
