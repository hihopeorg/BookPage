package com.anlia.pageturn.factory;

import com.anlia.pageturn.utils.BitmapUtils;
import com.anlia.pageturn.utils.ScreenUtils;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.PixelMap;

/**
 * 页面内容工厂类：制作图像集合型内容
 */
public class PicturesPageFactory extends PageFactory {

    private Context context;

    public int style;//集合类型
    public final static int STYLE_IDS = 1;//drawable目录图片集合类型
    public final static int STYLE_URIS = 2;//手机本地目录图片集合类型

    private int[] picturesIds;

    private final Paint nullPaint = new Paint();

    /**
     * 初始化drawable目录下的图片id集合
     */
    public PicturesPageFactory(Context context, int[] pictureIds) {
        this.context = context;
        this.picturesIds = pictureIds;
        this.style = STYLE_IDS;
        if (pictureIds.length > 0) {
            hasData = true;
            pageTotal = pictureIds.length;
        }
    }

    private String[] picturesUris;

    /**
     * 初始化本地目录下的图片uri集合
     */
    public PicturesPageFactory(Context context, String[] picturesUris) {
        this.context = context;
        this.picturesUris = picturesUris;
        this.style = STYLE_URIS;
        if (picturesUris.length > 0) {
            hasData = true;
            pageTotal = picturesUris.length;
        }
    }

    @Override
    public void drawPreviousBitmap(PixelMap bitmap, int pageNum) {
        Texture texture = new Texture(bitmap);
        Canvas canvas = new Canvas(texture);
        canvas.drawPixelMapHolder(new PixelMapHolder(getBitmapByIndex(pageNum - 2)), 0, 0, nullPaint);
    }

    @Override
    public void drawCurrentBitmap(PixelMap bitmap, int pageNum) {
        Texture texture = new Texture(bitmap);
        Canvas canvas = new Canvas(texture);
        canvas.drawPixelMapHolder(new PixelMapHolder(getBitmapByIndex(pageNum - 1)), 0, 0, nullPaint);
    }

    @Override
    public void drawNextBitmap(PixelMap bitmap, int pageNum) {
        Texture texture = new Texture(bitmap);
        Canvas canvas = new Canvas(texture);
        canvas.drawPixelMapHolder(new PixelMapHolder(getBitmapByIndex(pageNum)), 0, 0, nullPaint);
    }

    @Override
    public PixelMap getBitmapByIndex(int index) {
        if (hasData) {
            switch (style) {
                case STYLE_IDS:
                    return getBitmapFromIds(index);
                case STYLE_URIS:
                    return getBitmapFromUris(index);
                default:
                    return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 从id集合获取bitmap
     */
    private PixelMap getBitmapFromIds(int index) {
        Resource resource = null;
        try {
            resource = context.getResourceManager().getResource(picturesIds[index]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BitmapUtils.resourceToBitmap(resource,
                ScreenUtils.getScreenWidth(context),
                ScreenUtils.getScreenHeight(context)
        );
    }

    /**
     * 从uri集合获取bitmap
     */
    private PixelMap getBitmapFromUris(int index) {
        return null;//这个有空再写啦，大家可自行补充完整
    }
}
