package com.anlia.pageturn.factory;

import ohos.media.image.PixelMap;

/**
 * 页面内容工厂类
 */
public abstract class PageFactory {

    public boolean hasData = false;//是否含有数据
    public int pageTotal = 0;//页面总数

    public PageFactory() {
    }

    /**
     * 绘制上一页bitmap
     */
    public abstract void drawPreviousBitmap(PixelMap bitmap, int pageNum);

    /**
     * 绘制当前页bitmap
     */
    public abstract void drawCurrentBitmap(PixelMap bitmap, int pageNum);

    /**
     * 绘制下一页bitmap
     */
    public abstract void drawNextBitmap(PixelMap bitmap, int pageNum);

    /**
     * 通过索引在集合中获取相应内容
     */
    public abstract PixelMap getBitmapByIndex(int index);
}
